package cl.kevin.test.awto.kevintestawto.controllers;

import cl.kevin.test.awto.kevintestawto.json.request.LogRequest;
import cl.kevin.test.awto.kevintestawto.services.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/logs")
public class LogController {

  @Autowired
  private LogService logService;

  @PostMapping
  public ResponseEntity<?> create (@Valid @RequestBody LogRequest log) {
    return ResponseEntity.status(HttpStatus.CREATED).body(logService.create(log));
  }

  @GetMapping("/hashtag/{hashtagId}")
  public ResponseEntity<?> getAll (@PathVariable ("hashtagId") Integer hashtagId) {
    return ResponseEntity.ok(this.logService.getLogsByHashtagId(hashtagId));
  }
}
