package cl.kevin.test.awto.kevintestawto.json.request;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class HashTagRequest {

  private Integer id;
  private String description;

}
