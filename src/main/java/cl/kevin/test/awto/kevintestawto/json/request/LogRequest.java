package cl.kevin.test.awto.kevintestawto.json.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import java.util.Set;

@Data
@AllArgsConstructor
public class LogRequest {
  private String host;
  private String details;
  private Set<String> hashTags;
}
