package cl.kevin.test.awto.kevintestawto.controllers;

import cl.kevin.test.awto.kevintestawto.json.request.HashTagRequest;
import cl.kevin.test.awto.kevintestawto.json.request.LogRequest;
import cl.kevin.test.awto.kevintestawto.services.HashTagService;
import cl.kevin.test.awto.kevintestawto.services.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/hashtags")
public class HashTagController {

  @Autowired
  private HashTagService hashTagService;

  @PutMapping()
  public ResponseEntity<?> create (@Valid @RequestBody HashTagRequest request) {
    return ResponseEntity.status(HttpStatus.ACCEPTED).body(hashTagService.update(request));
  }
}
