package cl.kevin.test.awto.kevintestawto.services;

import cl.kevin.test.awto.kevintestawto.entities.Log;
import cl.kevin.test.awto.kevintestawto.json.request.LogRequest;
import cl.kevin.test.awto.kevintestawto.json.response.LogResponse;

import java.util.List;

public interface LogService {
  LogResponse create(LogRequest log);

  List<LogResponse> getLogsByHashtagId(Integer hashTagId);
}
