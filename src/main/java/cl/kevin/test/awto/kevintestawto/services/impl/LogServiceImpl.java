package cl.kevin.test.awto.kevintestawto.services.impl;

import cl.kevin.test.awto.kevintestawto.entities.HashTag;
import cl.kevin.test.awto.kevintestawto.entities.HashTagLog;
import cl.kevin.test.awto.kevintestawto.entities.Log;
import cl.kevin.test.awto.kevintestawto.json.request.LogRequest;
import cl.kevin.test.awto.kevintestawto.json.response.HashTagResponse;
import cl.kevin.test.awto.kevintestawto.json.response.LogResponse;
import cl.kevin.test.awto.kevintestawto.repositories.HashTagLogRepository;
import cl.kevin.test.awto.kevintestawto.repositories.HashTagRepository;
import cl.kevin.test.awto.kevintestawto.repositories.LogRepository;
import cl.kevin.test.awto.kevintestawto.services.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class LogServiceImpl implements LogService {
  @Autowired
  private LogRepository logRepository;

  @Autowired
  private HashTagRepository hashTagRepository;

  @Autowired
  private HashTagLogRepository hashTagLogRepository;

  @Override
  @Transactional
  public LogResponse create(LogRequest logRequest) {
    Log log = new Log();
    Set<String> hashTags = logRequest.getHashTags();
    Set<HashTag> hashTagsRelated = new HashSet<>();
    Set<HashTagLog> listHashTagLog = new HashSet<>();
    List<HashTagResponse> listHashTagResponse = new ArrayList<>();
    log.setHost(logRequest.getHost());
    log.setDetails(logRequest.getDetails());
    log = logRepository.save(log);
    for (String hashTag : hashTags) {
      HashTagLog hashTagLog = new HashTagLog();
      HashTag newHashTag = new HashTag();
      String hashTagClean = hashTag.replaceAll("#", "");
      if (this.hashTagRepository.findByDescription(hashTagClean).isEmpty()) {
        newHashTag.setDescription(hashTagClean);
        newHashTag = this.hashTagRepository.save(newHashTag);
      } else {
        newHashTag = this.hashTagRepository.findByDescription(hashTagClean).get(0);
      }
      hashTagLog.setHashTag(newHashTag);
      hashTagLog.setLog(log);
      hashTagsRelated.add(newHashTag);
      listHashTagLog.add(hashTagLog);
    }

    this.hashTagLogRepository.saveAll(listHashTagLog);
    LogResponse logResponse = this.logToResponse(this.logRepository.findById(log.getId()).get());

    for (HashTag hashTag : hashTagsRelated) {
      HashTagResponse hashTagResponse = new HashTagResponse();
      hashTagResponse.setDescription(hashTag.getDescription());
      hashTagResponse.setId(hashTag.getId());
      listHashTagResponse.add(hashTagResponse);
    }
    logResponse.setHashTags(listHashTagResponse);
    return logResponse;
  }

  @Override
  public List<LogResponse> getLogsByHashtagId(Integer hashtagId) {
    List<LogResponse> response = new ArrayList<>();
    List<Log> listLog = this.logRepository.findLoggersByHashTagId(hashtagId);
    if (!listLog.isEmpty()) {
      for (Log log : listLog) {
        LogResponse logResponse = this.logToResponse(log);
        response.add(logResponse);
      }
    }
    return response;
  }

  private LogResponse logToResponse(Log newLogResponse) {
    LogResponse logResponse = new LogResponse();
    List<HashTagResponse> listHashTagResponse = new ArrayList<>();
    logResponse.setCreationDate(newLogResponse.getCreationDate());
    logResponse.setDetails(newLogResponse.getDetails());
    logResponse.setHost(newLogResponse.getHost());
    logResponse.setId(newLogResponse.getId());
    Iterator<HashTagLog> iterHashTagLog = newLogResponse.getHashTags().iterator();
    while (iterHashTagLog.hasNext()) {
      HashTagLog hashTagLog = iterHashTagLog.next();
      HashTagResponse hashTagResponse = new HashTagResponse();
      hashTagResponse.setDescription(hashTagLog.getHashTag().getDescription());
      hashTagResponse.setId(hashTagLog.getHashTag().getId());
      listHashTagResponse.add(hashTagResponse);
    }
    logResponse.setHashTags(listHashTagResponse);
    return logResponse;
  }
}
