package cl.kevin.test.awto.kevintestawto.repositories;

import cl.kevin.test.awto.kevintestawto.entities.Log;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface LogRepository extends JpaRepository<Log, Integer> {

  @Query(
          value="SELECT alog.* FROM aw_log alog " +
                  "inner join aw_hashtag_log ahlog on ahlog.log_id = alog.id " +
                  "where ahlog.hashtag_id = :hashTagId",
          nativeQuery = true
  )
  List<Log> findLoggersByHashTagId(Integer hashTagId);


}
