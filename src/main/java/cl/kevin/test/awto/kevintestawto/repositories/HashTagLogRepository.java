package cl.kevin.test.awto.kevintestawto.repositories;

import cl.kevin.test.awto.kevintestawto.entities.HashTagLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HashTagLogRepository extends JpaRepository<HashTagLog, Integer> {
}
