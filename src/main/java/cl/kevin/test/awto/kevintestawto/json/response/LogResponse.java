package cl.kevin.test.awto.kevintestawto.json.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogResponse {

  private Integer id;
  private Date creationDate;
  private String host;
  private String details;
  private List<HashTagResponse> hashTags;

}
