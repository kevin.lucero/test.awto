package cl.kevin.test.awto.kevintestawto.services;

import cl.kevin.test.awto.kevintestawto.json.request.HashTagRequest;
import cl.kevin.test.awto.kevintestawto.json.response.HashTagResponse;

public interface HashTagService {
  public HashTagResponse update(HashTagRequest request);
}
