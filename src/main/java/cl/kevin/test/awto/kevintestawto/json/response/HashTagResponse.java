package cl.kevin.test.awto.kevintestawto.json.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HashTagResponse {

  private Integer id;
  private String description;

}
