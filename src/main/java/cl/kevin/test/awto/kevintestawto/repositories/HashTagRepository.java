package cl.kevin.test.awto.kevintestawto.repositories;

import cl.kevin.test.awto.kevintestawto.entities.HashTag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HashTagRepository extends JpaRepository<HashTag, Integer> {

  List<HashTag> findByDescription(String description);

}
