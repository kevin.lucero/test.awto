package cl.kevin.test.awto.kevintestawto.services.impl;

import cl.kevin.test.awto.kevintestawto.entities.HashTag;
import cl.kevin.test.awto.kevintestawto.json.request.HashTagRequest;
import cl.kevin.test.awto.kevintestawto.json.response.HashTagResponse;
import cl.kevin.test.awto.kevintestawto.repositories.HashTagRepository;
import cl.kevin.test.awto.kevintestawto.services.HashTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HashTagServiceImpl implements HashTagService {

  @Autowired
  private HashTagRepository hashTagRepository;

  @Override
  public HashTagResponse update(HashTagRequest request) {
    HashTagResponse response = new HashTagResponse();
    HashTag hashTag = this.hashTagRepository.findById(request.getId()).get();
    hashTag.setDescription(request.getDescription());
    hashTag = this.hashTagRepository.save(hashTag);
    response.setId(hashTag.getId());
    response.setDescription(hashTag.getDescription());
    return response;
  }
}
