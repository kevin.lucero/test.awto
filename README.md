# Logs management microservice
Author: Kevin Lucero

Test Awto: [Click Here](https://drive.google.com/file/d/1NyPrkmjUdbBpyC89h5zuFOGghmVU0DPc/view)


#
## Dependencies
This project needs to have java 11 installed and an IDE like Eclipse or Intellij.

Docker and Docker Compose: Docker is the only dependency  to use a Mysql database and phpMyAdmin.


 ```
 docker-compose up -d
 ```

- create database 'awto_log'

- import this file [awto_log.sql](https://drive.google.com/file/d/1oOU6A1RCOZMUt4eGXcf5ZlCzmfia3YkK/view?usp=sharing) using phpMyAdmin

_Mysql DB uses the port 3306._

_PhpMyAdmin use the port 7300._

- ### user: root

- ### password: awto
#

## Project settings

- Clone project: `git clone git@gitlab.com:kevin.lucero/test.awto.git`
- Install maven Package: `mvn install`
- Api doc with Swagger UI: [http://localhost:8080/swagger-ui/](http://localhost:8080/swagger-ui/<>)

